package djf;

import djf.ui.*;
import djf.components.*;
import djf.controller.AppFileController;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import static djf.ui.AppGUI.CLASS_NONBORDERED_PANE2;
import static djf.ui.AppGUI.CLASS_PROMPT_LABEL;
import static djf.ui.AppGUI.CLASS_PROMPT_LABEL2;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.util.Optional;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.StageStyle;
import properties_manager.InvalidXMLFileFormatException;

/**
 * This is the framework's JavaFX application. It provides the start method that
 * begins the program initialization, which delegates component initialization
 * to the application-specific child class' hook function.
 *
 * @author Richard McKenna & Keyu LIu
 * @version 1.0
 */
public abstract class AppTemplate extends Application implements AppDataComponent {

    // THIS IS THE APP'S FULL JavaFX GUI. NOTE THAT ALL APPS WOULD
    // SHARE A COMMON UI EXCEPT FOR THE CUSTOM WORKSPACE
    protected AppGUI gui;

    // THIS CLASS USES A COMPONENT ARCHITECTURE DESIGN PATTERN, MEANING IT
    // HAS OBJECTS THAT CAN BE SWAPPED OUT FOR OTHER COMPONENTS
    // THIS APP HAS 4 COMPONENTS
    // THE COMPONENT FOR MANAGING CUSTOM APP DATA
    protected AppDataComponent dataComponent;

    // THE COMPONENT FOR MANAGING CUSTOM FILE I/O
    protected AppFileComponent fileComponent;

    // THE COMPONENT FOR THE GUI WORKSPACE
    protected AppWorkspaceComponent workspaceComponent;

    protected AppFileController fileController;

    // THIS METHOD MUST BE OVERRIDDEN WHERE THE CUSTOM BUILDER OBJECT
    // WILL PROVIDE THE CUSTOM APP COMPONENTS
    /**
     * This function must be overridden, it should initialize all of the
     * components used by the app in the proper order according to the
     * particular app's dependencies.
     */
    public abstract void buildAppComponentsHook();

    // COMPONENT ACCESSOR METHODS
    /**
     * Accessor for the data component.
     */
    public AppDataComponent getDataComponent() {
        return dataComponent;
    }

    /**
     * Accessor for the file component.
     */
    public AppFileComponent getFileComponent() {
        return fileComponent;
    }

    /**
     * Accessor for the workspace component.
     */
    public AppWorkspaceComponent getWorkspaceComponent() {
        return workspaceComponent;
    }

    /**
     * Accessor for the gui. Note that the GUI would contain the workspace.
     */
    public AppGUI getGUI() {
        return gui;
    }

    public static final String CLASS_BORDERED_PANE2 = "bordered_pane2";

    /**
     * This is where our Application begins its initialization, it will load the
     * custom app properties, build the components, and fully initialize
     * everything to get the app rolling.
     *
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) {
        // LET'S START BY INITIALIZING OUR DIALOGS

        Stage priStage = new Stage();
        HBox title = new HBox();
        VBox recentWork = new VBox();
        Label rctWork = new Label();
        Label titleLabel = new Label("Welcome to Code Check");
        Hyperlink exitLabel = new Hyperlink("X");
        rctWork.setText("RecentWork");
        GridPane codeCheckTM = new GridPane();
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
//        Rectangle2D bounds = screen.getVisualBounds();
//        priStage.setX(bounds.getMinX());
//        priStage.setY(bounds.getMinY());
        priStage.setWidth(1000);
        priStage.setHeight(600);

        priStage.setTitle("Welcome to Code Check");
        GridPane welcomePane = new GridPane();
        Scene SceneWel = new Scene(welcomePane, 1000, 600);
        SceneWel.getStylesheets().add(getClass().getResource("style/sc_style.css").toExternalForm());

        welcomePane.setPrefSize(1000, 800);
        welcomePane.getStyleClass().add(CLASS_BORDERED_PANE);

        welcomePane.add(title, 0, 0);

        welcomePane.add(recentWork, 0, 1);
        welcomePane.add(codeCheckTM, 1, 1);

        recentWork.setPrefSize(300, 600);
        codeCheckTM.setPrefSize(700, 600);
        title.getStyleClass().add(CLASS_NONBORDERED_PANE2);
        titleLabel.getStyleClass().add(CLASS_PROMPT_LABEL2);
        exitLabel.getStyleClass().add(CLASS_PROMPT_LABEL2);
        recentWork.getStyleClass().add(CLASS_BORDERED_PANE);
        rctWork.getStyleClass().add(CLASS_PROMPT_LABEL);
        codeCheckTM.getStyleClass().add(CLASS_BORDERED_PANE);

        Hyperlink newCodeHyperlink = new Hyperlink("Create New Code Check");
        recentWork.getChildren().add(rctWork);

        File[] fileList = getFileList("./work/");
        fileController = new AppFileController(this);
//display recent work hyperlink
        for (File file : fileList) {
            String recentWorkPath="./work/" + file.getName();
            File testFile = new File("./work/" + file.getName() + "/submissions/");
            if (testFile.isDirectory()) {
                System.out.println(file.getName());
                Hyperlink rcntWorkLinke = new Hyperlink(file.getName());
                recentWork.getChildren().add(rcntWorkLinke);
                rcntWorkLinke.setOnAction(e -> {
                    boolean saved;
                    File currentWorkFile;
                    //recent work place            
                    AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
                    messageDialog.init(primaryStage);
                    AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                    yesNoDialog.init(primaryStage);
                    PropertiesManager props = PropertiesManager.getPropertiesManager();
                    try {
                        boolean success = loadProperties(APP_PROPERTIES_FILE_NAME);

                        if (success) {
                            String appTitle = file.getName();
                            gui = new AppGUI(primaryStage, appTitle, this);
                            buildAppComponentsHook();
                            primaryStage.show();
                            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                            boolean continueToMakeNew = true;
                            if (continueToMakeNew) {
                                workspaceComponent.resetWorkspace();
                                dataComponent.resetData();
                                workspaceComponent.reloadWorkspace(dataComponent);
                                workspaceComponent.activateWorkspace(gui.getAppPane());
                                
                            
                            
                            //waiting for fixing
                            
                            
                            File bbFile=new File(recentWorkPath);
                            gui.getFileController().handleRecentWork(bbFile);
                            
                            
                            
                            
                            
                            
                            

                                saved = false;
                                currentWorkFile = null;
                                gui.updateToolbarControls(saved);
                                dialog.show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
                            }
                            
                            primaryStage.setOnCloseRequest(event -> {
                                Alert closeAlert = new Alert(AlertType.CONFIRMATION);
                                closeAlert.setTitle("Confirmation Dialog");
                                closeAlert.setHeaderText(null);
                                closeAlert.setContentText("Are you sure close it?");

                                Optional<ButtonType> result = closeAlert.showAndWait();
                                if (result.get() == ButtonType.OK) {
                                    primaryStage.close();
                                } else {
                                    event.consume();
                                }
                            });
                            priStage.close();
                        }
                    } catch (Exception ex) {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
                    }

                });

            } else {
                //System.out.println("load recent work error: "+file.getName());
            }
        }

        Image image = new Image("djf/CodeCheck.png", 650, 200, true, true);
        ImageView CCTM = new ImageView(image);

        title.getChildren().add(titleLabel);
        title.getChildren().add(exitLabel);
        exitLabel.translateXProperty().set(760);
        exitLabel.translateYProperty().set(0);

        codeCheckTM.add(CCTM, 0, 0);
        codeCheckTM.add(newCodeHyperlink, 0, 1);

        CCTM.translateXProperty().set(0);
        CCTM.translateYProperty().set(40);
        newCodeHyperlink.translateXProperty().set(250);
        newCodeHyperlink.translateYProperty().set(300);

        priStage.setScene(SceneWel);
        priStage.initStyle(StageStyle.UNDECORATED);
        priStage.show();

        exitLabel.setOnAction(e -> {

            AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
            messageDialog.init(primaryStage);
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.init(primaryStage);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            try {
                // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
                // AND THE CUSTOM UI STUFF FOR THE WORKSPACE
                boolean success = loadProperties(APP_PROPERTIES_FILE_NAME);

                if (success) {
                    // GET THE TITLE FROM THE XML FILE
                    //   String appTitle = "Code Check - ";
                    // BUILD THE BASIC APP GUI OBJECT FIRST
                    gui = new AppGUI(primaryStage, "", this);

                    // THIS BUILDS ALL OF THE COMPONENTS, NOTE THAT
                    // IT WOULD BE DEFINED IN AN APPLICATION-SPECIFIC
                    // CHILD CLASS
                    buildAppComponentsHook();

                    // NOW OPEN UP THE WINDOW
                    primaryStage.show();
                    primaryStage.setOnCloseRequest(event -> {
                        Alert closeAlert = new Alert(AlertType.CONFIRMATION);
                        closeAlert.setTitle("Confirmation Dialog");
                        closeAlert.setHeaderText(null);
                        closeAlert.setContentText("Are you sure close it?");

                        Optional<ButtonType> result = closeAlert.showAndWait();
                        if (result.get() == ButtonType.OK) {
                            primaryStage.close();
                        } else {
                            event.consume();
                        }
                    });
                    priStage.close();
                }
            } catch (Exception ex) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
            }

        });

        newCodeHyperlink.setOnAction(e -> {

            TextInputDialog alerWindow = new TextInputDialog();

            alerWindow.setHeaderText(null);
            alerWindow.setTitle("new Code Check");
            alerWindow.setContentText("Please enter new Code Check name:");

            File currentWorkFile;

            boolean saved;
// Traditional way to get the response value.
            Optional<String> result = alerWindow.showAndWait();
            if (result.isPresent()) {
                AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
                messageDialog.init(primaryStage);
                AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
                yesNoDialog.init(primaryStage);
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                try {
                    // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
                    // AND THE CUSTOM UI STUFF FOR THE WORKSPACE
                    boolean success = loadProperties(APP_PROPERTIES_FILE_NAME);

                    if (success) {
                        // GET THE TITLE FROM THE XML FILE
                        String appTitle = result.get();
                        // BUILD THE BASIC APP GUI OBJECT FIRST
                        gui = new AppGUI(primaryStage, appTitle, this);

                        File ccfoldor = new File("./work/" + result.get() + "/");
                        ccfoldor.mkdir();
                        File bbfoldor = new File("./work/" + result.get() + "/blackboard/");
                        bbfoldor.mkdir();
                        File smfoldor = new File("./work/" + result.get() + "/submissions/");
                        smfoldor.mkdir();
                        File pjfoldor = new File("./work/" + result.get() + "/projects/");
                        pjfoldor.mkdir();
                        File codefoldor = new File("./work/" + result.get() + "/code/");
                        codefoldor.mkdir();

                        // THIS BUILDS ALL OF THE COMPONENTS, NOTE THAT
                        // IT WOULD BE DEFINED IN AN APPLICATION-SPECIFIC
                        // CHILD CLASS
                        buildAppComponentsHook();

                        // NOW OPEN UP THE WINDOW
                        primaryStage.show();

                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();

                        // WE MAY HAVE TO SAVE CURRENT WORK
                        boolean continueToMakeNew = true;
//                if (!saved) {
//                    // THE USER CAN OPT OUT HERE WITH A CANCEL
//                    continueToMakeNew = promptToSave();
//                }
// IF THE USER REALLY WANTS TO MAKE A NEW COURSE
                        if (continueToMakeNew) {
                            // RESET THE WORKSPACE
                            workspaceComponent.resetWorkspace();

                            // RESET THE DATA
                            dataComponent.resetData();

                            // NOW RELOAD THE WORKSPACE WITH THE RESET DATA
                            workspaceComponent.reloadWorkspace(dataComponent);

                            // MAKE SURE THE WORKSPACE IS ACTIVATED
                            workspaceComponent.activateWorkspace(gui.getAppPane());

                            // WORK IS NOT SAVED
                            saved = false;
                            currentWorkFile = null;

                            // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                            // THE APPROPRIATE CONTROLS
                            gui.updateToolbarControls(saved);

                            // TELL THE USER NEW WORK IS UNDERWAY
                            dialog.show(props.getProperty(NEW_COMPLETED_TITLE), props.getProperty(NEW_COMPLETED_MESSAGE));
                        }

                        primaryStage.setOnCloseRequest(event -> {
                            Alert closeAlert2 = new Alert(AlertType.CONFIRMATION);
                            closeAlert2.setTitle("Confirmation Dialog");
                            closeAlert2.setHeaderText(null);
                            closeAlert2.setContentText("Are you sure close it?");

                            Optional<ButtonType> result2 = closeAlert2.showAndWait();
                            if (result2.get() == ButtonType.OK) {
                                primaryStage.close();
                            } else {
                                event.consume();
                            }
                        });

                        priStage.close();
                    }
                } catch (Exception ex) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
                }
            }
        });

    }

    private static File[] getFileList(String dirPath) {
        //           File dir = new File(dirPath); 
        File workFolder = new File("./work/");

        File[] fileList = workFolder.listFiles(new FilenameFilter() {
            public boolean accept(File workFolder, String name) {
                return workFolder.isDirectory();
            }
        });
        return fileList;
    }

    public void mainUI() {

    }

    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     *
     * @param propertiesFileName The XML file containing properties to be loaded
     * in order to initialize the UI.
     *
     * @return true if the properties file was loaded successfully, false
     * otherwise.
     */
    public boolean loadProperties(String propertiesFileName) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
            return false;
        }
    }

    public void resetData(File recentFile) {


    }
}
 